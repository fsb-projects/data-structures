function BinarySearchTree(n){
this.value=n;
this.right=null;
this.left=null
this.len=1
}

BinarySearchTree.prototype.insert=function(n){
    //si n es mayor que el valor lo vamos a incluir a la derecha
    //si n es menor o igual a el valor lo vamos a incluir a la izquierda.
    n>this.value?this.aux1(n,"right"):this.aux1(n,"left")
}
BinarySearchTree.prototype.aux1=function(n,mode){
    if (this[mode]!=null){
        //si a la derecha existe un nodo, llamamos a su funcion insert
        this[mode].insert(n)
        }else{
        //si a la derecha no existe nada, creamos un nodo
        this[mode]=new BinarySearchTree(n)
            this.len++
        }
}
BinarySearchTree.prototype.contains=function(n){
    if (n==this.value){
        return true
    }else{
        if(n>this.value){
        return this.right!=null?this.right.contains(n):false
        }else{
        return this.left!=null?this.left.contains(n):false  
        }
    }

}
BinarySearchTree.prototype.depthFirstForEach=function(fn,option="in-order"){
    if (option=="in-order"){
        this.left!=null && this.left.depthFirstForEach(fn)
        fn(this.value)
        this.right!=null && this.right.depthFirstForEach(fn)
    }else if (option=="pre-order"){
        fn(this.value)
        this.left!=null && this.left.depthFirstForEach(fn,option)
        this.right!=null && this.right.depthFirstForEach(fn,option)
    }else{
        this.left!=null && this.left.depthFirstForEach(fn,option)
        this.right!=null && this.right.depthFirstForEach(fn,option)
        fn(this.value)
    }

}
BinarySearchTree.prototype.breadthFirstForEach=function(fn,list=[],puntero=0){
    list[puntero]==undefined?list[puntero]=[this.value]:list[puntero].push(this.value)
    this.left!=null && this.left.breadthFirstForEach(fn,list,puntero+1)
    this.right!=null && this.right.breadthFirstForEach(fn,list,puntero+1)
    if (puntero==0){
     list.reduce((arr,arr2)=>arr.concat(arr2)).map(val=>fn(val))
    } 
}
BinarySearchTree.prototype.size=function(){
return this.len
}