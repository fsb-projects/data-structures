function QueueCirc(n){
    this.data=new Uint8Array(n)
    this.end=0
    this.start=0
}
QueueCirc.prototype.enqueue=function(value){
    if(value<=0 || value>=255 || typeof(value)!="number"){
        throw Error ("Only take 0,255 numbers")
    }
    if(this.data[this.end]){
        throw Error ("full Bufffer")
        }
    this.data[this.sum("end")]=value
}
QueueCirc.prototype.dequeue=function(){
    var data=this.data[this.start]
    if (data){
        this.data[this.sum("start")]=undefined
        return data
    }else{
        throw Error ("Empty queue")
    }
}
QueueCirc.prototype.sum=function(point){
    data=this[point]
    this[point]=this[point]==this.data.length-1?0:this[point]+1
    return data
}