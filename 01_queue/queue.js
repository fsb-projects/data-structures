// Queue Without linked list
/* 
function Queue(){
this.len= 0;
this.data= [];
this.inicio= 0
this.final= 0;
}
Queue.prototype.enqueue=function(valor){
this.data[this.len==0?this.final:++this.final]=valor
this.len++
}
Queue.prototype.dequeue=function(){
    if(this.len==0){
       return undefined
    }else {
        --this.len
        return this.data[this.len==0?this.inicio:this.inicio++]
    }
}
Queue.prototype.size=function(){return this.len}*/

//Queue with linked list implementation
function Queue(){
    this.len= 0;
    this.data=new LinkedList();
}
Queue.prototype.enqueue=function(valor){
   this.data.addToTail(valor)
   this.len++ }

Queue.prototype.dequeue=function(){
    this.len!=0?this.len--:this.len
    var element=this.data.removeHead()
    return element==null?undefined:element
    
}
Queue.prototype.size=function(){return this.len}
