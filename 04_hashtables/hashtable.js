/*This is an Argentinian way to use a hash table with a linked list to avoid colisions */


function HashTable(n=35){
    this.keys={};
    this.numBuckets=n
}

HashTable.prototype.set=function(key,value){
    /* function that takes a key and a value and set it into our hashtable as an array key value*/
    if(typeof(key)!="string"){
        throw TypeError ('Keys must be strings')
    }
    var ind=this.hash(key)
    if(!this.keys[ind]){
        this.keys[ind]=new LinkedList()
    }
    this.keys[ind].addToHead([key,value])
    
}
HashTable.prototype.get=function(key){
    /* */
    var ind=this.hash(key)
    if (this.keys[ind]==null) return null
    var search=this.keys[ind].search((values)=>values[0]==key)
    return search!=null?search[1]:null

}
HashTable.prototype.hasKey=function(key){
   return this.get(key)?true:false
}
HashTable.prototype.hash=function(string){
    /* function that hash a string and return the hash */
return string.split("").reduce((c,e)=>c+e.charCodeAt(),0)%this.numBuckets

}
