function Node(valor){
    this.value=valor;
    this.next=null;
    this.previous=null
}

function LinkedList(){
    this.head=false;
    this.tail=false;
    this.len=0;

}
LinkedList.prototype.addToTail=function(valor){
    var newValue=new Node(valor)
    if(this.len==0){
        this.head=newValue
    }else {
        newValue.previous=this.tail
        this.tail.next=newValue
    }
    this.tail=newValue
    this.len++
}
LinkedList.prototype.addToHead=function(valor){
    var newValue=new Node(valor)
    if(this.len==0){
        this.tail=newValue
    }else {
        newValue.next=this.head
        this.head.previous=newValue
    }
    this.head=newValue
    this.len++
}
LinkedList.prototype.removeHead=function(){
    if(this.len==0){
        return null
    }
    var nodo=this.head.value
    if(this.len==1){
        this.tail=null
    }
    else if(this.len>1){
        this.head.next.previous=null
    }
    this.head=this.head.next
    this.len--
    return nodo
}
LinkedList.prototype.removeTail=function(){
    if(this.len==0){
        return null
    }
    var nodo=this.tail.value
    if(this.len==1){
        this.head=null
    }
    else if(this.len>1){
        this.tail.previous.next=null
    }
    this.tail=this.tail.previous
    this.len--
    return nodo
}
LinkedList.prototype.search=function(fn){
    var funcion=fn
    if (typeof(fn)!="function"){
        funcion=function(value){
            return fn===value
        }
    }
    var nodo=this.head
    while (nodo!==null){
        if(funcion(nodo.value)){
            return nodo.value
        }else{
            nodo=nodo.next
        }
    }
    return null
}